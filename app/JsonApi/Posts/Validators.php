<?php

namespace App\JsonApi\Posts;

use CloudCreativity\LaravelJsonApi\Validation\AbstractValidators;
use CloudCreativity\LaravelJsonApi\Contracts\Validation\ValidatorInterface;
use Illuminate\Validation\Validator;

class Validators extends AbstractValidators
{

    /**
     * The include paths a client is allowed to request.
     *
     * @var string[]|null
     *      the allowed paths, an empty array for none allowed, or null to allow all paths.
     */
    protected $allowedIncludePaths = [];

    /**
     * The sort field names a client is allowed send.
     *
     * @var string[]|null
     *      the allowed fields, an empty array for none allowed, or null to allow all fields.
     */
    protected $allowedSortParameters = [];

    /**
     * The filters a client is allowed send.
     *
     * @var string[]|null
     *      the allowed filters, an empty array for none allowed, or null to allow all.
     */
    protected $allowedFilteringParameters = [];

    /**
     * Get resource validation rules.
     *
     * @param mixed|null $record
     *      the record being updated, or null if creating a resource.
     * @param array $data
     *      the data being validated
     * @return array
     */

    // public function withValidator(Validator $validator)
    // {
    //     $validator->after(function (Validator $validator){
    //         dd($validator) ;
    //     });
        
    // }

    /**
     * @param array $document
     * @return \CloudCreativity\LaravelJsonApi\Contracts\Validation\ValidatorInterface
     */
    public function create(array $document): ValidatorInterface
    {
        $validator = parent::create($document);
        $validator->after(function (Validator $validator) {
            $modified_request = $validator->getData();
            $modified_request ['password'] = bcrypt($validator->getDisplayableValue('password',$modified_request['password']));
            $validator->setData($modified_request);
        });
        return $validator;
    }



    protected function rules($record, array $data): array
    {
        return [
            'title' => "required|string|min:3",
        ];
    }

    /**
     * Get query parameter validation rules.
     *
     * @return array
     */
    protected function queryRules(): array
    {
        return [
            //
        ];
    }

}
