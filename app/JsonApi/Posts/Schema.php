<?php

namespace App\JsonApi\Posts;

use Neomerx\JsonApi\Schema\SchemaProvider;

class Schema extends SchemaProvider
{

    /**
     * @var string
     */
    protected $resourceType = 'posts';

    /**
     * @param \App\Post $resource
     *      the domain record being serialized.
     * @return string
     */
    public function getId($resource)
    {
        return (string) $resource->getRouteKey();
    }

    /**
     * @param \App\Post $resource
     *      the domain record being serialized.
     * @return array
     */
    public function getAttributes($resource)
    {
        return [
            'title'         => $resource->title,
            'description'   => $resource->description,
            'title'         => $resource->title,
            'user'          => $resource->user ,
            'createdAt'     => $resource->created_at,
            'updatedAt'     => $resource->updated_at,
        ];
    }

    public function getRelationships($post, $isPrimary, array $includeRelationships)
    {
        return [
            'user' => [
                self::DATA => function () use ($post) {
                    return $post->user;
                },
            ],
        ];
    }

}
