<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\User ;
use CloudCreativity\LaravelJsonApi\Facades\JsonApi;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
JsonApi::register('v1')->withNamespace('Api')->routes(function ($api) {
    // $api->resource('posts');
    $api->resource('posts')->relationships(function ($relations) {
        $relations->hasOne('user');
    });

    $api->resource('users');
});

// Route::post('/tokens/create', function (Request $request) {
//     $token = User::find(1)->createToken('app-token');
//     return response()->json(['token' => $token->plainTextToken , 'user' => User::find(1)]);
    
// });


// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
